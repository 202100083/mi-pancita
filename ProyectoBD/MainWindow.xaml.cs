﻿using ProyectoBD.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProyectoBD
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Usuario> usuarios = new List<Usuario>();
        private Usuario usuario_temporal;
        public MainWindow()
        {
            InitializeComponent();
            using (var refresh = new DataBaseContext())
            {
                var persona = refresh.Usuarios.ToList();
                tblUsuarios.ItemsSource = persona;
            }
        }

        private void ButtonGuardar(object sender, RoutedEventArgs e)
        {
            Usuario usuario = new Usuario();
            usuario.Id = 0;
            usuario.Nombres = txtNombres.Text;
            usuario.Apellidos = txtApellidos.Text;
            usuario.Correo = txtCorreo.Text;
            usuarios.Add(usuario);
            tblUsuarios.Items.Refresh();

            using (var puerto = new DataBaseContext())
            {
                puerto.Usuarios.Add(usuario);
                puerto.SaveChanges();
                puerto.Dispose();
            }

            using (var refresh = new DataBaseContext())
            {
                var persona = refresh.Usuarios.ToList();
                tblUsuarios.ItemsSource = persona;
            }


        }

        private void tblUsuarios_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void tblUsuarios_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DataBaseContext conexion = new DataBaseContext();
                for (int i = 0; i < tblUsuarios.SelectedItems.Count; i++)
                {
                    Usuario usuario = (Usuario)tblUsuarios.SelectedItems[i];
                    conexion.Usuarios.Remove(usuario);
                }
                conexion.SaveChanges();
                conexion.Dispose();
            }
        }

        private void tblUsuarios_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
        }

        private void clik_doble(object sender, MouseButtonEventArgs e)
        {
            usuario_temporal = (Usuario)tblUsuarios.SelectedItem;
            if (usuario_temporal == null)
            {
                return;
            }
            txtNombres.Text = usuario_temporal.Nombres;
            txtApellidos.Text = usuario_temporal.Apellidos;
            txtCorreo.Text = usuario_temporal.Correo;

            boton_guardar.Content = "guardar cambios";


        }

        private void click_derecho(object sender, MouseButtonEventArgs e)
        {
            


        }


        private void Button_actualizar(object sender, RoutedEventArgs e)
        {
            if (usuario_temporal == null)
            {
                return;
            }
            Usuario usuario = usuario_temporal;
            usuario.Nombres = txtNombres.Text;
            usuario.Apellidos = txtApellidos.Text;
            usuario.Correo = txtCorreo.Text;
            tblUsuarios.Items.Refresh();

            using (var puerto = new DataBaseContext())
            {
                puerto.Usuarios.Update(usuario);
                puerto.SaveChanges();
                puerto.Dispose();
            }
        }
    }
}

